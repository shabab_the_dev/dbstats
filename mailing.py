import os
import sys
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

CWD = os.path.dirname(os.path.abspath(__file__)) + '/'


class Mailings(object):
    """This class reads a configuration file and sends email

    Attributes:
        mail (SMTP_SSL): SMTP ssl object for sending and recieving email.
    """

    def __init__(self):

        self.mail = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com','587')
        self.mail.ehlo()
        self.mail.starttls()
        self.mail.ehlo()
        self.mail.login('AKIAJDMJ26PY5HIMODFA', 'AjgzG86BAGNVAyZbo/O5zMpn7n6I3nvXdmvICyLzeVX4')
        self.sender = 'kscan@karza.in'

    def send_mail(self, subject, recipients, sender=None, html_body=[], plain_body=[]):
        """Sends mail from sender described in config.yml to recievers passed as html arguments

        Args:
            subject (TYPE): Subject for Email
            recipients (TYPE): list or individual email address of reciepent
            sender (str, optional): email of sender
            html_body (list, optional): list of messages encolsed by html tags
            plain_body (list, optional): list of messages in text
        """
        msg = MIMEMultipart()
        msg['Subject'] = subject
        #msg['from'] = self.config['sender']

        html_body = map(str, html_body)
        plain_body = map(str, plain_body)

        msg.attach(MIMEText('\n'.join(plain_body), 'plain'))
        msg.attach(MIMEText('<hr>'.join(html_body), 'html'))

        sender = sender if sender else self.sender

        self.mail.sendmail(sender, recipients, msg.as_string())

    def __del__(self):
        self.mail.close()


if __name__ == '__main__':

    mailer = Mailings()

    mailer.send_mail('Mapping for DB:{} Collection:{}',['vaishnavi.s@karza.in'], plain_body=['Just a test email'])
