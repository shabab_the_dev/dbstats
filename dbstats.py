#!/usr/bin/env python
from mailing import Mailings
from prettytable import PrettyTable
import pymongo
from pprint import pprint
import json
import sys
from tqdm import tqdm
import argparse


class DbStats():

    def __init__(self, maxDepth=15):

        self.keyCounts = {}
        self.maxDepth = maxDepth
        self.dbStatus = False

        self.ref_dct = {}
        self.dtype_map = {'Int64': 'INTEGER',
                          'int': 'INTEGER',
                          'unicode': 'STRING',
                          'datetime': 'DATE',
                          'float': 'FLOAT',
                          'bool': 'BOOLEAN'
                          }

    def initDb(self, connType, dbName, collectionName, criteria=None, projection=None, mail=None):

        self.dbStatus = True
        self.connType = connType
        self.dbName = dbName
        self.collectionName = collectionName
        self.criteria = criteria
        self.projection = projection
        self.mail = mail

        if self.connType == 'local':
            self.connection = pymongo.MongoClient(
                "mongodb://localhost", connectTimeoutMS=None, serverSelectionTimeoutMS=60000)
        elif self.connType == 'cluster':
            self.connection = pymongo.MongoClient(
                "mongodb://10.64.0.11:27017,10.64.1.11:27017", connectTimeoutMS=None, serverSelectionTimeoutMS=60000)
        else:
            print "Invalid Connection Type"
            sys.exit(0)

        self.db = self.connection[self.dbName]
        self.collection = self.db[self.collectionName]

    def traverse(self, key, values, _id, parent_type, depth=0):

        if depth >= self.maxDepth:
            return

        if key:
            if not self.keyCounts.has_key(key):
                self.keyCounts[key] = {
                    'exists': 0,
                    'min': float('inf'),
                    'max': float('-inf'),
                    'sum': 0,
                    'count': 0,
                    'min_doc_id': '',
                    'max_doc_id': '',
                    'dtypes': set()
                }

            if (parent_type == list and type(values) == list) or parent_type != list:
                self.keyCounts[key]['dtypes'].add(type(values))
                self.keyCounts[key]['exists'] += 1

        if isinstance(values, list):
            for row in values:
                self.traverse(key, row, _id, type(values), depth + 1)

        elif isinstance(values, dict):
            for k, v in values.iteritems():
                if key is None:
                    self.traverse(k, v, _id, type(values), depth + 1)
                else:
                    self.traverse("{key1}.{key2}".format(
                        key1=key, key2=k), v, _id, type(values), depth + 1)

        elif isinstance(values, unicode) or isinstance(values, str) or isinstance(values, int) or isinstance(values, float) or isinstance(values, long):
            values = unicode(values)
            l = len(values)
            old_min = self.keyCounts[key]['min']
            old_max = self.keyCounts[key]['max']

            if l < old_min:
                self.keyCounts[key]['min'] = l
                self.keyCounts[key]['min_doc_id'] = str(_id)

            if l > old_max:
                self.keyCounts[key]['max'] = l
                self.keyCounts[key]['max_doc_id'] = str(_id)

            self.keyCounts[key]['sum'] += l
            self.keyCounts[key]['count'] += 1
            return
        else:
            return

    def printStats(self):

        ptable = PrettyTable()
        ptable.field_names = ["key", "dtypes",
                              "exists", "not exists", "min", "max", "avg", "min_doc_id", "max_doc_id"]
        for key in sorted(self.keyCounts.iterkeys()):
            values = self.keyCounts[key]

            dtypes = values.get("dtypes")
            dtypes = list(dtypes)
            dtypes = ', '.join([type.__name__ for type in dtypes])

            key_count = values.get("count")
            exists_count = values.get("exists")
            not_exists = self.recordCounts - exists_count
            min_ = values.get('min')
            max_ = values.get('max')
            minDocId = values.get('min_doc_id')
            maxDocId = values.get('max_doc_id')

            if key_count:
                avg = values.get("sum") / float(key_count)
                avg = round(avg, 2)
            else:
                avg = "N/A"

            row = [key, dtypes, exists_count, not_exists, min_, max_, avg, minDocId, maxDocId]

            if key != '_id':
                self.ref_dct.update({key: dtypes})

            ptable.add_row(row)

        ptable.align = 'l'

        print ptable
        if self.mail != None:
            return self.dump_standard_output()
        else:
            return None

    def generate_standard_key(self, key):
        splitted_key = key.split('.')
        new_key = ''
        for idx, k in enumerate(splitted_key):

            sk = '.'.join(splitted_key[:idx + 1])
            dtype = self.ref_dct[sk]
            if dtype == 'list':
                new_key += '{}:[]:'.format(k)
            elif dtype == 'dict':
                new_key += '{}:'.format(k)
            else:
                new_key += '{}'.format(k)

        return new_key

    def dump_standard_output(self):
        output = '{\n'
        for key, dtype in self.ref_dct.items():

            if 'dflag' in key or 'kflag' in key:
                continue

            if dtype in ['list', 'dict']:
                continue

            target_type = self.dtype_map.get(dtype, 'NULL')
            standard_name = self.generate_standard_key(key)
            target_key = standard_name if '[]' in standard_name else standard_name.replace(':', '_')

            output += '''  "{standard_name}"  : |\n    "ignore_attribute": false,\n    "target_key": "{target_key}",\n    "target_type": "{target_type}",\n    "transformations": [],\n    "validations": [],\n    "comments":""\n    $,
            \n'''.format(standard_name=standard_name, target_key=target_key, target_type=target_type).replace('|', '{').replace('$', '}')
        output = output.strip().strip(',')
        output += '\n}'
        return output

    def aggregateDb(self):
        if not self.dbStatus:
            raise Exception("DB not initialized")

        count = self.collection.find(self.criteria).count()
        cursor = self.collection.find(self.criteria, self.projection)
        self.aggregate(cursor, count)

    def aggregateArray(self, data):
        if not isinstance(data, list):
            raise TypeError("data must be of type 'list'")

        count = len(data)
        self.aggregate(data, count)

    def aggregate(self, iterator, count):
        self.recordCounts = count
        print "Total Records Count : ", self.recordCounts

        with tqdm(total=self.recordCounts) as pbar:
            for data in iterator:
                self.traverse(None, data, data.get("_id"), None)
                pbar.update(1)

        print "Total Keys found : ", len(self.keyCounts)

    def getStats(self):
        return self.keyCounts


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("connectionType", choices=["local", "cluster"], help="Specify Cluster or Local", type=str)
    parser.add_argument("db", help="Specify DB Name", type=str)
    parser.add_argument("collection", help="Specify Collection Name", type=str)
    parser.add_argument("--mail", help="Specify if mail to be send", type=str)

    parser.add_argument("-q", "--query", help="Specify Query JSON", type=json.loads)
    parser.add_argument("-p", "--project", help="Specify Project JSON", type=json.loads)
    parser.add_argument("-l", "--limit", help="Specify limit", type=int)
    parser.add_argument("-s", "--skip", type=int)

    args = parser.parse_args()

    print args.mail

    obj = DbStats()
    obj.initDb(args.connectionType, args.db, args.collection, args.query, args.project, args.mail)
    obj.aggregateDb()
    # obj.aggregateArray([{"a" : 1}])
    ret = obj.printStats()

    if ret != None:
        print ret
        mailer = Mailings()
        mailer.send_mail('Mapping for DB:{} Collection:{}'.format(args.db,args.collection),['vaishnavi.s@karza.in'], plain_body=[ret])
